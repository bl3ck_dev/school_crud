@extends('layouts.app')
@section('css')
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">
@endsection
@section('content')
    <div class="table-responsive">
        <div class="table-wrapper">
            <div class="table-title">
                <div class="row">
                    <div class="col-sm-8"><h2>Student <b>Details</b></h2></div>
                    <div class="col-sm-4">
                        <button type="button" class="btn btn-info addNew" data-toggle="modal" data-target="#addNew"><i class="fa fa-plus"></i> Add New</button>

                    </div>
                </div>
            </div>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>S/N </th>
                        <th >First Name</th>
                        <th >Last Name</th>
                        <th >Phone No.</th>
                        <th >Email </th>
                        <th >Gender </th>
                        <th >Actions </th>
                    </tr>
                </thead>
                <tbody>
                    @if (count($students) > 0)
                        @php $number = 1; @endphp
                        @foreach($students as $student)
                            <tr>
                                <td> {{ $number }} </td>
                                <td >{{ $student->firstName }}</td>
                                <td>{{ $student->lastName }}</td>
                                <td>{{ $student->phone_no }} </td>
                                <td>{{ $student->email }}</td>
                                <td>{{ $student->gender }}</td>
                                <td>
                                    <a  data-toggle="modal" data-target="#editRecord-{{ $number }}"><i class="material-icons edit" style="color: green;">edit </i> </a>
                                    <a  data-toggle="modal" data-target="#deleteRecord-{{ $number }}"><i class="material-icons" style="color: red;">delete </i> </a>

                                    <!-- edit Modal HTML -->
                                    <div class="modal fade" id="editRecord-{{ $number }}">
                                        <div class="modal-dialog modal-sm" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">Edit {{ $student->firstName }} Details</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <form action="students/{{$student->id}}" method='POST'>
                                                    @csrf
                                                    @method('PUT')
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                <input type="text" value="{{ $student->firstName }}" class="form-control" name="firstName" required placeholder="First Name">
                                                            </div><br>
                                                            <div class="input-group">
                                                                <input type="text" value="{{ $student->lastName }}" class="form-control" name="lastName" required placeholder="Last Name">
                                                            </div><br>
                                                            <div class="input-group">
                                                                <input type="text" value="{{ $student->phone_no }}" class="form-control" name="phone_no" required placeholder="contact">
                                                            </div><br>
                                                            <div class="input-group">
                                                                <input type="email" value="{{ $student->email }}" class="form-control" name="email" required placeholder="email">
                                                            </div><br>
                                                            <div class="input-group">
                                                                <select name="gender" style="width:100%;">
                                                                    <option value="{{ $student->gender }}" selected hidden>{{ $student->gender }}</option>
                                                                    <option > Male </gender>
                                                                    <option> Female </gender>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                            <button type="submit" class="btn btn-primary">Update Details</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- delete Modal HTML -->
                                    <div class="modal fade" id="deleteRecord-{{ $number }}">
                                        <div class="modal-dialog modal-delete">
                                            <div class="modal-content">
                                                <div class="modal-header flex-column">
                                                    <div class="icon-box">
                                                        <i class="material-icons">clear</i>
                                                    </div>
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                </div>
                                                <div class="modal-body">
                                                    <p>Do you really want to delete <strong class="text-danger"> {{  $student->firstName ." ". $student->lastName }}</strong> records? This process cannot be undone.</p>
                                                </div>
                                                <div class="modal-footer justify-content-center">
                                                    <form action="students/{{$student->id}}" method="POST">
                                                        @method('DELETE')@csrf
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                                        <button type="submit" class="btn btn-danger">Delete</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </td>
                            </tr>
                            @php
                                $number++;
                            @endphp
                        @endforeach
                    @else
                        <tr>
                            <div class="text text-danger"><strong> This table have no record </strong></div>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
        <div class='pagination justify-content-center'>
            {{  $students->links()  }}
        </div>
    </div>
@endsection

@section('modal')
<!-- Add New Record Modal  -->
<div class="modal" id="addNew" tabindex="-1" role="dialog">
    <form action="{{ route('students.store') }}" method="POST">
    @csrf
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"><small>add</small> <strong>Student</strong></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="auto-form-wrapper">

                        <div class="form-group">
                            <div class="input-group">
                                <input type="text" class="form-control @error('firstName') is-invalid @enderror" name="firstName" id="firstName" placeholder="First Name">
                                @error('firstName')
                                    <span class="invalid-feedback" role="alert"> <strong>{{ $message }}</strong></span>
                                @enderror
                            </div><br>
                            <div class="input-group">
                                <input type="text" class="form-control @error('lastName') is-invalid @enderror" name="lastName" id="lastName" placeholder="Last Name">
                                @error('lastName')
                                    <span class="invalid-feedback" role="alert"> <strong>{{ $message }}</strong></span>
                                @enderror
                            </div><br>
                            <div class="input-group">
                                <input type="number" class="form-control @error('phone_no') is-invalid @enderror" name="phone_no" id="phone_no" placeholder="contact">
                                @error('phone_no')
                                    <span class="invalid-feedback" role="alert"> <strong>{{ $message }}</strong></span>
                                @enderror
                            </div><br>
                            <div class="input-group">
                                <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" id="email" placeholder="email">
                                @error('email')
                                    <span class="invalid-feedback" role="alert"> <strong>{{ $message }}</strong></span>
                                @enderror
                            </div><br>
                            <div class="input-group ">
                                <select name="gender" style="width:100%;" class=" form-control @error('gender') is-invalid @enderror">
                                    <option> Male </gender>
                                    <option> Female </gender>
                                </select>
                                @error('gender')
                                    <span class="invalid-feedback" role="alert"> <strong>{{ $message }}</strong></span>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </form>
</div>

@if (session('success'))
    <script type="text/javascript">
        $(document).ready(function(){
            $('#confirm').modal('show')
        })
    </script>
@elseif (session('error'))
    <script type="text/javascript">
        $(document).ready(function(){
            $('#failed').modal('show')
        })
    </script>
@endif
@endsection
