@extends('layouts.app')
@section('css')
    <link rel="stylesheet" href="{{ asset('css/login.css') }}">
@endsection

@section('content')
<div class="container" style="max-width: 60%;">
        <div class="row justify-content-center " >
            <div class="col-lg-6 col-md-6 login-box">
                <div class="col-lg-12 login-title"><small> RESET PASSWORD</small> </div>

                <div class="col-lg-12 login-form">
                    <div class="col-lg-12 login-form">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                        <form method="POST" action="{{ route('password.email') }}">
                            @csrf
                            <div class="form-group">
                                <label class="form-control-label ">EMAIL</label>
                                <input type="email" name="email" id="email"  class="form-control @error('email') is-invalid @enderror" value="{{ old('email')}}">
                                @error('email')
                                    <span class="invalid-feedback" role="alert"> <strong>{{ $message }}</strong></span>
                                @enderror
                            </div>

                            <div class="col-md-12 loginbttm">
                                <div class="col-md-10 login-btm login-button">
                                    <button type="submit" class="btn btn-outline-primary">{{ __('Send Password Reset Link') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-lg-3 col-md-2"></div>
            </div>
        </div>
    </div>

@endsection
