@extends('layouts.app')
@section('css')
    <link rel="stylesheet" href="{{ asset("css/login.css") }}">
@endsection

@section('content')
    <div class="container" style="max-width: 60%;">
        <div class="row justify-content-center " >
            <div class="col-lg-6 col-md-6 login-box">
                <div class="col-lg-12 login-key"><i class="fa fa-key" aria-hidden="true"></i></div>
                <div class="col-lg-12 login-title"> ADMIN PANEL </div>

                <div class="col-lg-12 login-form">
                    <div class="col-lg-12 login-form">
                        <form method="POST" action="{{ route('login') }}">
                            @csrf
                            <div class="form-group">
                                <label class="form-control-label ">EMAIL</label>
                                <input type="email" name="email" id="email"  class="form-control @error('email') is-invalid @enderror" value="{{ old('email')}}">
                            </div>
                            <div class="form-group">
                                <label class="form-control-label">PASSWORD</label>
                                <input type="password" id="password" name="password" class="form-control @error('email')is-invalid @enderror">
                                @error('email')
                                    <span class="invalid-feedback" role="alert"> <strong>{{ $message }}</strong></span>
                                @enderror

                            </div>
                            <div class="form-group">
                                <div class="col-md-11 login-btm login-button">
                                    <button type="submit" class="btn btn-primary btn-block">LOGIN</button>
                                </div>
                            </div>

                            <div class="col-md-12 loginbttm">
                                <div class="col-md-11 login-btm login-button">
                                    @if (Route::has('password.request'))
                                        <a class="btn btn-link" href="{{ route('password.request') }}">
                                            {{ __('Forgot Password? ') }}Click here to reset
                                        </a>
                                    @endif
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-lg-3 col-md-2"></div>
            </div>
        </div>
    </div>
@endsection
