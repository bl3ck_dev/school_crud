@extends('layouts.app')
@section('css')
    <link rel="stylesheet" href="{{ asset("css/login.css") }}">
@endsection

@section('content')
<div class="container " style="max-width: 60%;">
    <div class="row justify-content-center " >
        <div class="col-lg-6 col-md-6 login-box">

            <div class="col-lg-12 login-title">
                <small> REGISTRATION FORM </small>
            </div>

            <div class="col-lg-12 login-form">
                <div class="col-lg-12 login-form">
                    <form method="POST" action="{{ route('register') }}" class="form-detail">
                        @csrf
                        <div class="form-group">
                            <label class="form-control-label">Full Name</label>
                            <input type="text" id="name" name='name' class="form-control @error('name') is-invalid @enderror" value="{{ old('name')}}" placeholder="John Doe">
                            @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label class="form-control-label">Email</label>
                            <input type="email" id="email" name='email' class="form-control @error('email') is-invalid @enderror" value="{{ old('email') }}"placeholder="john@doe.org" >
                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label class="form-control-label">Password</label>
                            <input type="password" id="password" name='password' class="form-control @error('password') is-invalid @enderror" >
                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label class="form-control-label">Confirm Password</label>
                            <input type="password" id="password-confirm" name='password_confirmation' class="form-control" >
                        </div>

                        <div class="col-md-12 loginbttm">
                            <div class="col-md-8 login-btm login-button">
                                <button type="submit" class="btn btn-outline-primary">REGISTER</button>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
            <div class="col-lg-3 col-md-2"></div>
        </div>
    </div>
</div>
@endsection
